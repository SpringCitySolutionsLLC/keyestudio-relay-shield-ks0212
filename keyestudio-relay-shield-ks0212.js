// keyestudio-relay-shield-ks0212.js
//
// Copyright (c) 2020-2021 Spring City Solutions LLC and other contributors
// Licensed under the Apache License, Version 2.0
// KEYESTUDIO (tm) is a trademark of Shenzhen KEYES DIY Robot co., Ltd.
//
// https://www.keyestudio.com/keyestudio-rpi-4channel-relay-5v-shield-for-raspberry-pi-ce-certification-p0475-p0475.html
// https://wiki.keyestudio.com/index.php/KS0212_keyestudio_RPI_4-channel_Relay_Shield
// https://gitlab.com/SpringCitySolutionsLLC/keyestudio-relay-shield-ks0212
// https://npmjs.org/package/node-red-contrib-keyestudio-relay-shield-ks0212
// https://flows.nodered.org/node/node-red-contrib-keyestudio-relay-shield-ks0212
// https://nodered.org/
// Links for Songle SRD-05VDC-SL-C relay:
// http://www.songlerelay.com/Product_index_id_19.htm
// http://www.songlerelay.com/Public/Uploads/20161104/581c81ac16e36.pdf
//
module.exports = function(RED) {
    "use strict";
    var execSync = require('child_process').execSync;
    var exec = require('child_process').exec;
    var spawn = require('child_process').spawn;
    var fs = require('fs');

    var testCommand = __dirname + '/testgpio.py'
    var gpioCommand = __dirname + '/nrgpio';
    var allOK = true;

    try {
        execSync(testCommand);
    } catch (err) {
        allOK = false;
        RED.log.warn("keyestudio-relay-shield-ks0212 : " + RED._("keyestudio-relay-shield-ks0212.errors.ignorenode"));
    }

    // the magic to make python print stuff immediately
    process.env.PYTHONUNBUFFERED = 1;

    function RelayJ2(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;

        function inputlistener(msg, send, done) {
            var out = Number(msg.payload);
            if (
                (msg.payload === "set") ||
                (msg.payload === "true") ||
                (msg.payload === "T") ||
                (msg.payload === "H") ||
                (msg.payload === "on") ||
                (Number(msg.payload) == 1)
            ) { out = 1; } else { out = 0; }
            if (RED.settings.verbose) { node.log("out: " + out); }
            if (node.child !== null) {
                node.child.stdin.write(out + "\n", () => {
                    if (done) { done(); }
                });
                node.status({ fill: "green", shape: "dot", text: msg.payload.toString() });
            } else {
                node.error(RED._("keyestudio-relay-shield-ks0212.errors.pythoncommandnotfound"), msg);
                node.status({ fill: "red", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.not-running" });
            }
        }

        if (allOK === true) {
            node.child = spawn(gpioCommand, [node.out, 7, 0]);
            node.status({ fill: "green", shape: "dot", text: node.level });
            node.running = true;

            node.on("input", inputlistener);

            node.child.stdout.on('data', function(data) {
                if (RED.settings.verbose) { node.log("out: " + data + " :"); }
            });

            node.child.stderr.on('data', function(data) {
                if (RED.settings.verbose) { node.log("err: " + data + " :"); }
            });

            node.child.on('close', function(code) {
                node.child = null;
                node.running = false;
                if (RED.settings.verbose) { node.log(RED._("keyestudio-relay-shield-ks0212.status.closed")); }
                if (node.finished) {
                    node.status({ fill: "grey", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.closed" });
                    node.finished();
                } else { node.status({ fill: "red", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.stopped" }); }
            });

            node.child.on('error', function(err) {
                if (err.errno === "ENOENT") { node.error(RED._("keyestudio-relay-shield-ks0212.errors.commandnotfound")); } else if (err.errno === "EACCES") { node.error(RED._("keyestudio-relay-shield-ks0212.errors.commandnotexecutable")); } else { node.error(RED._("keyestudio-relay-shield-ks0212.errors.error") + ': ' + err.errno); }
            });
        } else {
            node.status({ fill: "grey", shape: "dot", text: "keyestudio-relay-shield-ks0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("keyestudio-relay-shield-ks0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.closed" });
            if (node.child != null) {
                node.finished = done;
                node.child.stdin.write("close 7");
                node.child.kill('SIGKILL');
            } else { done(); }
        });

    }
    RED.nodes.registerType("RelayJ2", RelayJ2);

    function RelayJ3(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;

        function inputlistener(msg, send, done) {
            var out = Number(msg.payload);
            if (
                (msg.payload === "set") ||
                (msg.payload === "true") ||
                (msg.payload === "T") ||
                (msg.payload === "H") ||
                (msg.payload === "on") ||
                (Number(msg.payload) == 1)
            ) { out = 1; } else { out = 0; }
            if (RED.settings.verbose) { node.log("out: " + out); }
            if (node.child !== null) {
                node.child.stdin.write(out + "\n", () => {
                    if (done) { done(); }
                });
                node.status({ fill: "green", shape: "dot", text: msg.payload.toString() });
            } else {
                node.error(RED._("keyestudio-relay-shield-ks0212.errors.pythoncommandnotfound"), msg);
                node.status({ fill: "red", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.not-running" });
            }
        }

        if (allOK === true) {
            node.child = spawn(gpioCommand, [node.out, 15, 0]);
            node.status({ fill: "green", shape: "dot", text: node.level });
            node.running = true;

            node.on("input", inputlistener);

            node.child.stdout.on('data', function(data) {
                if (RED.settings.verbose) { node.log("out: " + data + " :"); }
            });

            node.child.stderr.on('data', function(data) {
                if (RED.settings.verbose) { node.log("err: " + data + " :"); }
            });

            node.child.on('close', function(code) {
                node.child = null;
                node.running = false;
                if (RED.settings.verbose) { node.log(RED._("keyestudio-relay-shield-ks0212.status.closed")); }
                if (node.finished) {
                    node.status({ fill: "grey", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.closed" });
                    node.finished();
                } else { node.status({ fill: "red", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.stopped" }); }
            });

            node.child.on('error', function(err) {
                if (err.errno === "ENOENT") { node.error(RED._("keyestudio-relay-shield-ks0212.errors.commandnotfound")); } else if (err.errno === "EACCES") { node.error(RED._("keyestudio-relay-shield-ks0212.errors.commandnotexecutable")); } else { node.error(RED._("keyestudio-relay-shield-ks0212.errors.error") + ': ' + err.errno); }
            });
        } else {
            node.status({ fill: "grey", shape: "dot", text: "keyestudio-relay-shield-ks0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("keyestudio-relay-shield-ks0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.closed" });
            if (node.child != null) {
                node.finished = done;
                node.child.stdin.write("close 15");
                node.child.kill('SIGKILL');
            } else { done(); }
        });

    }
    RED.nodes.registerType("RelayJ3", RelayJ3);

    function RelayJ4(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;

        function inputlistener(msg, send, done) {
            var out = Number(msg.payload);
            if (
                (msg.payload === "set") ||
                (msg.payload === "true") ||
                (msg.payload === "T") ||
                (msg.payload === "H") ||
                (msg.payload === "on") ||
                (Number(msg.payload) == 1)
            ) { out = 1; } else { out = 0; }
            if (RED.settings.verbose) { node.log("out: " + out); }
            if (node.child !== null) {
                node.child.stdin.write(out + "\n", () => {
                    if (done) { done(); }
                });
                node.status({ fill: "green", shape: "dot", text: msg.payload.toString() });
            } else {
                node.error(RED._("keyestudio-relay-shield-ks0212.errors.pythoncommandnotfound"), msg);
                node.status({ fill: "red", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.not-running" });
            }
        }

        if (allOK === true) {
            node.child = spawn(gpioCommand, [node.out, 31, 0]);
            node.status({ fill: "green", shape: "dot", text: node.level });
            node.running = true;

            node.on("input", inputlistener);

            node.child.stdout.on('data', function(data) {
                if (RED.settings.verbose) { node.log("out: " + data + " :"); }
            });

            node.child.stderr.on('data', function(data) {
                if (RED.settings.verbose) { node.log("err: " + data + " :"); }
            });

            node.child.on('close', function(code) {
                node.child = null;
                node.running = false;
                if (RED.settings.verbose) { node.log(RED._("keyestudio-relay-shield-ks0212.status.closed")); }
                if (node.finished) {
                    node.status({ fill: "grey", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.closed" });
                    node.finished();
                } else { node.status({ fill: "red", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.stopped" }); }
            });

            node.child.on('error', function(err) {
                if (err.errno === "ENOENT") { node.error(RED._("keyestudio-relay-shield-ks0212.errors.commandnotfound")); } else if (err.errno === "EACCES") { node.error(RED._("keyestudio-relay-shield-ks0212.errors.commandnotexecutable")); } else { node.error(RED._("keyestudio-relay-shield-ks0212.errors.error") + ': ' + err.errno); }
            });
        } else {
            node.status({ fill: "grey", shape: "dot", text: "keyestudio-relay-shield-ks0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("keyestudio-relay-shield-ks0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.closed" });
            if (node.child != null) {
                node.finished = done;
                node.child.stdin.write("close 31");
                node.child.kill('SIGKILL');
            } else { done(); }
        });

    }
    RED.nodes.registerType("RelayJ4", RelayJ4);

    function RelayJ5(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;

        function inputlistener(msg, send, done) {
            var out = Number(msg.payload);
            if (
                (msg.payload === "set") ||
                (msg.payload === "true") ||
                (msg.payload === "T") ||
                (msg.payload === "H") ||
                (msg.payload === "on") ||
                (Number(msg.payload) == 1)
            ) { out = 1; } else { out = 0; }
            if (RED.settings.verbose) { node.log("out: " + out); }
            if (node.child !== null) {
                node.child.stdin.write(out + "\n", () => {
                    if (done) { done(); }
                });
                node.status({ fill: "green", shape: "dot", text: msg.payload.toString() });
            } else {
                node.error(RED._("keyestudio-relay-shield-ks0212.errors.pythoncommandnotfound"), msg);
                node.status({ fill: "red", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.not-running" });
            }
        }

        if (allOK === true) {
            node.child = spawn(gpioCommand, [node.out, 37, 0]);
            node.status({ fill: "green", shape: "dot", text: node.level });
            node.running = true;

            node.on("input", inputlistener);

            node.child.stdout.on('data', function(data) {
                if (RED.settings.verbose) { node.log("out: " + data + " :"); }
            });

            node.child.stderr.on('data', function(data) {
                if (RED.settings.verbose) { node.log("err: " + data + " :"); }
            });

            node.child.on('close', function(code) {
                node.child = null;
                node.running = false;
                if (RED.settings.verbose) { node.log(RED._("keyestudio-relay-shield-ks0212.status.closed")); }
                if (node.finished) {
                    node.status({ fill: "grey", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.closed" });
                    node.finished();
                } else { node.status({ fill: "red", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.stopped" }); }
            });

            node.child.on('error', function(err) {
                if (err.errno === "ENOENT") { node.error(RED._("keyestudio-relay-shield-ks0212.errors.commandnotfound")); } else if (err.errno === "EACCES") { node.error(RED._("keyestudio-relay-shield-ks0212.errors.commandnotexecutable")); } else { node.error(RED._("keyestudio-relay-shield-ks0212.errors.error") + ': ' + err.errno); }
            });
        } else {
            node.status({ fill: "grey", shape: "dot", text: "keyestudio-relay-shield-ks0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("keyestudio-relay-shield-ks0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "keyestudio-relay-shield-ks0212.status.closed" });
            if (node.child != null) {
                node.finished = done;
                node.child.stdin.write("close 37");
                node.child.kill('SIGKILL');
            } else { done(); }
        });

    }
    RED.nodes.registerType("RelayJ5", RelayJ5);

}