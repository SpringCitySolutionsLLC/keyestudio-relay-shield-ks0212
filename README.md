node-red-contrib-keyestudio-relay-shield-ks0212
=====================

A set of <a href="http://nodered.org">Node-RED</a> nodes to control the four relays on a
<a href="https://wiki.keyestudio.com/index.php/KS0212_keyestudio_RPI_4-channel_Relay_Shield">
Keyestudio KS0212 relay shield</a>
using the RPi.GPIO python library that is part of Raspbian.

If you need more generic control of arbitrary GPIO pins, perhaps
for a different relay shield, then consider the
<a href="https://flows.nodered.org/node/node-red-node-pi-gpio">node-red-node-pi-gpio</a> node.

This node is based upon node-red-node-pi-gpio and provides plug and play use
of the relays on the shield.

Simply plug in the shield, install this Node-RED node, the relays just work
with no configuration required.

See the <a href="https://gitlab.com/SpringCitySolutionsLLC/keyestudio-relay-shield-ks0212/-/wikis/home">wiki</a> for more details.

## Install

You can watch the Youtube video describing how to set up the board:
<a href="https://youtu.be/VkkeTJV-4gc">https://youtu.be/VkkeTJV-4gc</a>

Either use the Node-RED Menu - Manage Palette option to install, or run the following
command in your Node-RED user directory - typically `~/.node-red`

        npm i node-red-contrib-keyestudio-relay-shield-ks0212

The python library may also work with other distros running on
a Pi (like Ubuntu or Debian) - you will need to install the PIGPIO
package and run the following commands in order to gain full access to
the GPIO pins as this ability is not part of the default distro.
This is NOT necessary on Raspbian.

        sudo apt-get install python-pip python-dev
        sudo pip install RPi.GPIO
        sudo addgroup gpio
        sudo chown root:gpio /dev/gpiomem
        sudo adduser $USER gpio
        echo 'KERNEL=="gpiomem", NAME="%k", GROUP="gpio", MODE="0660"' | sudo tee /etc/udev/rules.d/45-gpio.rules
        sudo udevadm control --reload-rules && sudo udevadm trigger

## Usage

Here is a link to the Youtube video describing how to use the software:
<a href="https://youtu.be/iC77coWWsT0">https://youtu.be/iC77coWWsT0</a>

Also you might want to watch the Youtube video showing electronics test 
bench performance of the hardware:
<a href="https://youtu.be/5Al1RFTqQQM">https://youtu.be/5Al1RFTqQQM</a>

#### Input

 - `msg.payload` - *boolean | number | string*

`msg.payload` containing String set, String true, String T, String H,
String on, String 1, Boolean T, or a Numerical 1 will turn the relay on.

`msg.payload` of any other content will turn the relay off.

## Funding

Please consider becoming a patron of Spring City Solutions LLC
Your support directly funds the development of new drivers 
and nodes for Node-RED.

For the latest news, detailed daily dev logs are posted on 
the Patreon page.

https://www.patreon.com/springcitysolutions_nodered

## Links

Hardware Links:

Board Manufacturer Store:
https://www.keyestudio.com/keyestudio-rpi-4channel-relay-5v-shield-for-raspberry-pi-ce-certification-p0475-p0475.html

Board Manufacturer Wiki:
https://wiki.keyestudio.com/index.php/KS0212_keyestudio_RPI_4-channel_Relay_Shield

Links for the Songle SRD-05VDC-SL-C relay used on the board:
http://www.songlerelay.com/Product_show_id_539.htm
http://www.songlerelay.com/Public/Uploads/20161104/581c81ac16e36.pdf

Spring City Solutions Links:

Spring City Solutions Node-RED project page:
https://www.springcitysolutions.com/nodered

Spring City Solutions page for this node:
https://www.springcitysolutions.com/nodered-keyestudio-relay-shield

Spring City Solutions Node-RED project email:
nodered@springcitysolutions.com

Spring City Solutions Gitlab for this node:
https://gitlab.com/SpringCitySolutionsLLC/keyestudio-relay-shield-ks0212

Patreon Page:
https://www.patreon.com/springcitysolutions_nodered

Software Links:

Spring City Solutions Gitlab for this node:
https://gitlab.com/SpringCitySolutionsLLC/keyestudio-relay-shield-ks0212

npmjs for this node:
https://npmjs.org/package/node-red-contrib-keyestudio-relay-shield-ks0212

Node-RED flows for this node:
https://flows.nodered.org/node/node-red-contrib-keyestudio-relay-shield-ks0212

Documentation Links:

Gitlab wiki for this node (the master copy of list of links is here):
https://gitlab.com/SpringCitySolutionsLLC/keyestudio-relay-shield-ks0212/-/wikis/home

Youtube video "How to set up":
https://youtu.be/VkkeTJV-4gc 

Youtube video "How to use":
https://youtu.be/iC77coWWsT0 

Youtube video "Testing Results":
https://youtu.be/5Al1RFTqQQM

## Trademarks

KEYESTUDIO is a registered trademark of Shenzhen KEYES DIY Robot co., Ltd.

Raspberry Pi is a trademark of Raspberry Pi Trading

Node-RED and node.js are a trademark of the OpenJS Foundation in the United States

Python is a registered trademark of the Python Software Foundation.

Songle is a trademark of Zhejiang Songle Machinery Co., Ltd. Wenling City,Zhejiang CHINA

Peakmeter is a trademark of Shenzhen New Huayi Instrument Co., Ltd.

Rigol is a trademark of RIGOL TECHNOLOGIES CO., LTD.

"All trademarks are property of their respective owners"

## Copyrights and Licenses

Majority of code Copyright (c) 2020-2021 Spring City Solutions LLC and other
contributors and licensed under the Apache License, Version 2.0

NRGPIO.PY Copyright JS Foundation and other contributors and licensed under Apache
License, Version 2.0

Icon made from http://www.onlinewebfonts.com/icon aka Icon Fonts is licensed by CC BY 3.0
